const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("Estamos escuchando el puerto "+port);

//TEST
app.get('/apitechu/v1/hello',
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"salida GET /apitechu/v1/hello"});
  }
);

//POST LOGIN
app.post('/apitechu/v1/login',
  function(req, res){
    var message = {
      "mensaje":"Login incorrecto"
    };
    console.log("Body");
    console.log(req.body.email);
    console.log(req.body.password);
    var users = require('./users_login.json');
    if(req.body.email != null && req.body.password){
      for (user of users) {
        console.log("mail : "+user.email);
        if(user.email==req.body.email){
          console.log("found");
          console.log("password : "+user.password);
          if(user.password == req.body.password){
            message.mensaje = "Login correcto";
            message.userId = user.id;
            user.logged = true;
          }
        }
      }
    }

    writeUserDataToFile(users);
    res.send(message);
});

//POST LOGOUT
app.post('/apitechu/v1/logout',
  function(req, res){

    var message = {
      "mensaje":"Logout incorrecto"
    };
    console.log("Body");
    console.log(req.body.id);

    var users = require('./users_login.json');
    if(req.body.id != null){
      for (user of users) {
        console.log("userId : "+user.id);
        if(user.id==req.body.id && user.logged==true){
          console.log("found");
          message.mensaje = "Logout correcto";
          delete user.logged;
        }
      }
    }

    writeUserDataToFile(users);
    res.send(message);
});

//POST MONSTRUO
app.post('/apitechu/v1/monster/:p1/:p2',
  function(req, res){

    console.log("post /apitechu/v1/monster");
    console.log("Parameters");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

    res.send("post /apitechu/v1/monster");

  });

//GET USERS
app.get('/apitechu/v1/users',
  function(req, res){
    console.log("GET /apitechu/v1/users");

    console.log("Query String");
    console.log(req.query.$count);
    console.log(req.query.$top);

    var users = require('./usuarios.json');
    if(req.query.$top != null){
      users = users.slice(0,req.query.$top);
      //users.splice(req.query.$top, users.length - req.query.$top);
    }
    //res.sendFile('usuarios.json', {root: __dirname});
    if(req.query.$count != null && req.query.$count == "true"){
      users.push({
        "count" : users.length
      });
    }
    res.send(users);
  });

//DELETE USERS
app.delete('/apitechu/v1/users/:id',
  function(req, res){
    console.log("DELETE /apitechu/v1/users/"+req.params.id);

    var users = require('./usuarios.json');
    //TE QUITA LA POSICION DE UN ELEMENTO INDICADO DETNTRO DE UN ARRAY
    var index = -1;
    var found = false;
    for (let user of users) {
      index++;
      console.log("index: "+index+", user : "+user.id);
      if(user.id==req.params.id){
        console.log("found");
        found = true;
        break;
      }
    }
    if(found){
      users.splice(index, 1);
      writeUserDataToFile(users);
      res.send({"msg":"Usuario deleteado con exito"});
    } else {
      res.send({"msg":"Usuario no encontrado"});
    }
    //delete users[pos];
    //users.splice(req.params.id - 1, 1);

  });

//POST USERS
app.post('/apitechu/v1/users',
  function(req, res){
    console.log("POST /apitechu/v1/users");

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
    }
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);

    res.send({"msg":"Usuario añadido con exito"});

  });

  function writeUserDataToFile(data){
    const fs = require('fs');
    var jsonUserData = JSON.stringify(data);
    fs.writeFile("./users_login.json",jsonUserData, "utf8",
      function(err){
          if(err){
            console.log(err);
          } else {
            console.log("Datos añadidos.");
          }
      });
  }
